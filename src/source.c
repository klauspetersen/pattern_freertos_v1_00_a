/*
 * datasource.c
 *
 *  Created on: 29/03/2013
 *      Author: Iris
 */

#include "stdint.h"
#include "FreeRTOS.h"
#include "source.h"
#include "subject.h"
#include "string.h"
#include "semphr.h"
#include "task.h"

typedef struct source{
	uint8_t strSynch[sourceMAX_STR_LEN];
	uint8_t strDeamon[sourceMAX_STR_LEN];
	xSemaphoreHandle xSemphSynch;
	xSemaphoreHandle xSemphDeamon;

	subj *pSubj;
} source_t;






int source_create(){

	source_t *pSource;

	pSource = sourceMALLOC(sizeof(source_t));

	pSource->pSubj = subj_create();

	pSource->strSynch[0] = '\0';

	pSource->strDeamon[0] = '\0';


	return 0;
}

int source_config(source_t *this,source_config_t *pxConfig){

	strncpy ((sourceSTR_TYPE)this->strSynch, "SYNCH_", sourceMAX_STR_LEN);
	strncat ((sourceSTR_TYPE)this->strSynch, (sourceSTR_TYPE)pxConfig->pcString, sourceMAX_STR_LEN-strlen((sourceSTR_TYPE)pxConfig->pcString));

	strncpy ((sourceSTR_TYPE)this->strDeamon, "DEAMON_", sourceMAX_STR_LEN);
	strncat ((sourceSTR_TYPE)this->strDeamon, (sourceSTR_TYPE)pxConfig->pcString, sourceMAX_STR_LEN-strlen((sourceSTR_TYPE)pxConfig->pcString));


	/* Trace macro's */

	if(pxConfig->bTraceEnable) {

		vDbgSemaphoreCreateBinary(this->xSemphSynch, (sourceSTR_TYPE)this->strSynch);

		vDbgSemaphoreCreateBinary(this->xSemphDeamon, (sourceSTR_TYPE)this->strDeamon);

		vDbgTraceSetISRProperties(pxConfig->id);
	}

	return 0;
}

int source_subscribe(source_t *this, void *func) {
	return subj_subscribe(this->pSubj, func);
}

int source_unsubscribe(source_t *this, void *func) {
	return subj_subscribe(this->pSubj, func);
}


int source_start(source_t *this){



	xTaskCreate( prvAdc840sSampleTask, ( signed char * ) "adc840s", adc840sSTACK_SIZE, NULL, adc840sPRIORITY, NULL );

}


int source_stop(source_t *this){

}
