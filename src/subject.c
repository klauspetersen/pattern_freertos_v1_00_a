/*
 * subject.c
 *
 *  Created on: 28/03/2013
 *      Author: Iris
 */
#include "stdlib.h"
#include "stddef.h"

/* DELETEME: */
#include "stdio.h"

#include "subject.h"

struct subj {
	void (*vptr)(void *);
	void *next;
	void *prev;
};

subj *subj_create(void) {
	subj *ptr;

	ptr = subjMALLOC(sizeof(subj));
	subjPRINT(("malloc: %p\r\n", ptr));

	ptr->vptr = NULL;
	ptr->next = NULL;
	ptr->prev = NULL;

	return ptr;
}

int subj_subscribe(subj *this, void *func) {
	subj *ptr;

	/* Walk the list */
	while (this->next != NULL ) {
		subjPRINT(("."));
		this = this->next;
	}

	/* If this is the top node, the vptr will be NULL */
	if (this->vptr == NULL ) {
		this->vptr = func;
	} else {

		ptr = this;

		/* Create a new node */
		this->next = subjMALLOC(sizeof(subj));
		subjPRINT(("malloc: %p\r\n", this->next));
		((subj *) this->next)->prev = ptr;
		this = this->next;
		this->vptr = func;
		this->next = NULL;
	}

	return 0;
}

int subj_notify(subj *this, void *arg) {
int i = 0;

	if (this->vptr != NULL ) {
		(this->vptr)(arg);
		i++;
	}

	while (this->next != NULL ) {
		this = this->next;
		(this->vptr)(arg);
		i++;
	}

	printf("Notified: %d\r\n", i);

	return i;
}

int subj_unsubscribe(subj *this, void *func) {
	subj *ptr;

	ptr = this;

	/* If the top node was a match */
	if (this->vptr == func) {
		if (this->next == NULL)	{
			/* This is also the last node left. It will be reset, but not free'd */
			this->vptr = NULL;
			this->prev = NULL;
		} else {
			/* this is not the last node left, so promote the next node to top node */
			ptr = this->next;
			this->vptr  = ((subj *)this->next)->vptr;
			this->prev  = NULL;
			this->next  = ((subj *)this->next)->next;

			subjPRINT(("free: %p\r\n", ptr));

			/* Stitch up the gap between the top node and the hole
			 * caused by the node promoted to top node by updating the
			 * ->prev value of the node on the other side of the gap.
			 */
			if(ptr->next != NULL) {
				((subj*)ptr->next)->prev = this;
			}
			subjFREE(ptr);
		}


	} else {

		/*walk the list until the end or a match is found */
		while (ptr->vptr != func) {

			if (ptr->next == NULL ) {
				/* If the end of the list was reached, return error */
				return -1;
			} else {
				ptr = ptr->next;
			}
		}

		/* The function has been located and it is not the top node*/

		if(ptr->next == NULL){
			/* This is the bottom node. Set the ->next of the previous to NULL */
			((subj *)ptr->prev)->next = NULL;

		} else {
			((subj *)ptr->prev)->next = ptr->next;
		}

		subjPRINT(("free: %p\r\n", ptr));
		subjFREE(ptr);

	}

	return 0;
}
