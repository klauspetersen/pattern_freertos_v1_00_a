/*
 * source.h
 *
 *  Created on: 29/03/2013
 *      Author: Iris
 */

#ifndef SOURCE_H_
#define SOURCE_H_


#include "stdint.h"
#include "stdbool.h"

#define sourceFREE(ptr) vPortFree(ptr)
#define sourceMALLOC(size) pvPortMalloc(size)
#define sourceVERBOSE 1
#define sourceSTR_TYPE char*

#define sourceMAX_STR_LEN 30

typedef struct {
	uint8_t * const pcString;
	uint32_t id;
	bool bTraceEnable;

} source_config_t;

typedef struct private private_t;

typedef struct source source_t;


int source_config(source_t *this, source_config_t *config);
int source_subscribe(source_t *this, void *func);
int source_unsubscribe(source_t *this, void *func);





#endif /* SOURCE_H_ */
